<?php include 'header.php'; ?>
<style>
	.fadein {
		opacity: 1;
	}
</style>
<div class="pagecontent">
	<div class="about-header the-fold fill-screen">
		<div class="about-header-inner">
			<?php include 'nav.php'; ?>
			<div class="about-intro">
				<!-- <div class="intro-bg" data-stellar-ratio=".9999"></div> -->
				<div data-stellar-ratio=".5" class="title-wrap">
				<h1>We love building</h1>
				<span>_unmistakable brands</span>
				</div><!-- title-wrap -->
			</div><!-- /.about-intro -->
		</div><!-- about-header-inner -->
	</div><!-- /.about-header -->	
	
<section class="our-clients">
	<div class="clients_title" > 
		Our clients span the world
	</div><!-- /.clients_title -->
	<h3 class="subtitle">We wouldn’t be able to do great work without great partners. Here are just a few.</h3>
	<div id="map">
		<div class="inner">
			<div class="pin ca"></div>
			<div class="pin la"></div>
			<div class="pin ny"></div>
			<div class="pin de"></div>
			<div class="pin gr"></div>
			<div class="pin ph"></div>
			<div class="pin au"></div>
		</div>
	</div>
</section>
<section class="what-we-do group">
	<div class="col-xs-24 col-sm-15">
		<div class="about-hatchit">
			<div class="about-heading">
				We love what we do, <br/>and we think you’ll love it too.
			</div><!-- /.about-heading -->
			<div class="about-subheading">
				So what exactly do we do? Good question.	
			<p>Over the years, we’ve often asked ourselves the same question... and every one of us has said something different. A design group. A digital agency. A ___________. To be honest, we love it this way. We don’t want to lock ourselves in the confines of one discipline. That’s why we’ve settled on calling Hatchit a creative firm. Fueled by curiosity and passion, we create beautiful things – experiences, feelings, visions, communications – from branding to web design to app development – to make businesses and brands better than they are. We craft products that have both brains and beauty and we have fun while doing it.</p>
			</div><!-- /.about-subheading -->
		</div><!-- /.about-hatchit -->
	</div><!-- /.col-xs-24 col-sm-15 -->
	<div class="col-xs-24 col-sm-9">
		<div class="our-services">
			<h2>Our primary services include:</h2>
			<ul>
				<li><span class="genericon genericon-checkmark"></span> <h3>Website design and development</h3></li>
				<li><span class="genericon genericon-checkmark"></span> <h3>Logo &amp; Branding</h3></li>
				<li><span class="genericon genericon-checkmark"></span> <h3>Web application design and development</h3></li>
			</ul>
			<br>
			<h2>Additional services include:</h2>
			<ul>
				<li><span class="genericon genericon-checkmark"></span> <h3>Digital and print graphic design (logos, business cards, etc.)</h3></li>
				<li><span class="genericon genericon-checkmark"></span> <h3>Consultation/training</h3></li>
			</ul>
			<br><br>
			<a class="btn btn-default contact-services" href="contact.php">Get in Touch</a>
		</div><!-- /.our-services -->
	</div><!-- /.col-xs-24 col-sm-9 -->
</section>

<section class="meet-team">
	<ul class="slides">
		<li rel="Nick Defelice">
			<div class="slider-container">
				<div class="photo col-xs-12"><img src="img/about-nick.png" alt=""></div><!-- photo -->
				<div class="content col-xs-12">
					<h2 class="slide-title">Meet Nick Defelice</h2>
					<span>Partner | Lead Developer</span>
					<div class="bio">
						<p>I’ve always found joy in learning and understanding new industries, business models, and how design can help solve problems. My average days are filled with sketching, designing, developing, meeting with clients, and collaborating with co-workers. In addition to design, I also have a passion for managing the technical side of G&M, including front end development, site migration, WordPress development, eCommerce development, and keeping up with SEO and web standards. </p>
					</div><!-- bio -->
					<div class="loves clearfix">
						<ul class="clearfix">
							<li><p>03 <span>loves</span></p></li>
							<li><img src="img/golf-icon.png" alt=""></li>
							<li><img src="img/travel-icon.png" alt=""></li>
							<li><img src="img/photography-icon.png" alt=""></li>
						</ul>
					</div><!-- loves -->
				</div><!-- content col-xs-6 -->
			</div>
		</li>

		<li rel="Chris Schwing">
			<div class="slider-container">
				<div class="photo col-xs-12"><img src="img/about-nick.png" alt=""></div><!-- photo -->
				<div class="content col-xs-12">
					<h2 class="slide-title">Meet Chris Schwing</h2>
					<span>Partner | Lead Developer</span>
					<div class="bio">
						<p>I’ve always found joy in learning and understanding new industries, business models, and how design can help solve problems. My average days are filled with sketching, designing, developing, meeting with clients, and collaborating with co-workers. In addition to design, I also have a passion for managing the technical side of G&M, including front end development, site migration, WordPress development, eCommerce development, and keeping up with SEO and web standards. </p>
					</div><!-- bio -->
					<div class="loves clearfix">
						<ul class="clearfix">
							<li><p>03 <span>loves</span></p></li>
							<li><img src="img/golf-icon.png" alt=""></li>
							<li><img src="img/travel-icon.png" alt=""></li>
							<li><img src="img/photography-icon.png" alt=""></li>
						</ul>
					</div><!-- loves -->
				</div><!-- content col-xs-6 -->
			</div>
		</li>

		<li rel="Jeremy Beyt">
			<div class="slider-container">
				<div class="photo col-xs-12"><img src="img/about-nick.png" alt=""></div><!-- photo -->
				<div class="content col-xs-12">
					<h2 class="slide-title">Meet Jeremy Beyt</h2>
					<span>Partner | Lead Developer</span>
					<div class="bio">
						<p>I’ve always found joy in learning and understanding new industries, business models, and how design can help solve problems. My average days are filled with sketching, designing, developing, meeting with clients, and collaborating with co-workers. In addition to design, I also have a passion for managing the technical side of G&M, including front end development, site migration, WordPress development, eCommerce development, and keeping up with SEO and web standards. </p>
					</div><!-- bio -->
					<div class="loves clearfix">
						<ul class="clearfix">
							<li><p>03 <span>loves</span></p></li>
							<li><img src="img/golf-icon.png" alt=""></li>
							<li><img src="img/travel-icon.png" alt=""></li>
							<li><img src="img/photography-icon.png" alt=""></li>
						</ul>
					</div><!-- loves -->
				</div><!-- content col-xs-6 -->
			</div>
		</li>

	</ul>
	<a href="#" class="prev-slide">
		<span></span>
		<p>Gabi Ferrara</p>
	</a>

	<a href="#" class="next-slide">
		<span></span>
		
	</a>
</section>

<?php include 'footer.php'; ?>
</div><!-- pagecontent -->