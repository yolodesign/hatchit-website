<?php include 'header.php'; ?>
<div id="contact-header">
	<?php include 'nav.php'; ?>
</div>
	<div id="leftcontact" class="col-xs-24 col-sm-12">
		<div id="leftcontent_content">
			<h1>Let's Work Together</h1>
			<p>The best way to get started is to fill out the form here, or contact us through <a href="mailto:hello@hatchit.co?Subject=Hello" target="_top">email</a> or telephone.  We’d love to start a conversation about your next project, no pressure here, just a friendly conversation.</p>
			<br/>
			<div id='crmWebToEntityForm'>
				<META HTTP-EQUIV ='content-type' CONTENT='text/html;charset = UTF-8'>  
				<form action='https://crm.zoho.com/crm/WebToLeadForm' name=WebToLeads931413000000070001 method='POST' onSubmit='javascript:document.charset="UTF-8"; return checkMandatery()' accept-charset='UTF-8'>  
				
				<input type='text' style='display:none;' name='xnQsjsdp' value='YdQ6p-WcCeRthYo@kRl79w$$'/>  
				<input type='hidden' name='zc_gad' id='zc_gad' value=''/>  
				<input type='text' style='display:none;' name='xmIwtLD' value='bw729Ud0j-KNU@uyPq3OMG-@B6-MHnNR'/>  
				<input type='text'  style='display:none;' name='actionType' value='TGVhZHM='/> 
				<input type='text' style='display:none;' name='returnURL' value='http&#x3a;&#x2f;&#x2f;yolodesign.com&#x2f;contact.php?msg=thankyou' /> 
				<br>
				
				<!--<input type='text' style='width:250px;'  maxlength='100' name='Company' />-->
				<input type='text' class="col-xs-24 col-sm-12" maxlength='80' name='Last Name' placeholder="Your Name" />
				<input type='text' class="right col-xs-24 col-sm-12" maxlength='100' name='Email' placeholder="Your Email Address" />
				<input type='text' class="col-xs-24 col-sm-12" maxlength='255' name='Website' placeholder="Your Current Website (if you have one)"/>
				<input type='text' class="right col-xs-24 col-sm-12" maxlength='255' name='Phone' placeholder="Your Phone Number"/>
				<!--<select style='width:250px;' name='Lead Source'>  <option value='-None-'>-None-</option>  <option value='Chat'>Chat</option>  <option value='Website - Other'>Website - Other</option>  <option value='Website - Google'>Website - Google</option>  <option value='Website - Facebook Ad'>Website - Facebook Ad</option>  <option value='Website - Facebook Other'>Website - Facebook Other</option>  <option value='Website - Referral'>Website - Referral</option>  <option value='Employee Referral'>Employee Referral</option>  <option value='External Referral'>External Referral</option>  <option value='Cold Call'>Cold Call</option>  <option value='Partner'>Partner</option>  <option value='Sales Mail Alias'>Sales Mail Alias</option>  <option value='Trade Show'>Trade Show</option>  </select>-->
				
				<textarea name='Description' maxlength='1000' width='250' height='250' placeholder="Tell us about your project.  How can we help out?"></textarea>
				<input type='submit'  value='Send Message' /> 
						
				<script> 
					var mndFileds=new Array('Last Name');
					var fldLangVal=new Array('Last Name');
					function reloadImg(){if(document.getElementById('imgid').src.indexOf('&d') !== -1 ){document.getElementById('imgid').src=document.getElementById('imgid').src.substring(0,document.getElementById('imgid').src.indexOf('&d'))+'&d'+new Date().getTime();}else{document.getElementById('imgid').src = document.getElementById('imgid').src+'&d'+new Date().getTime();}}function checkMandatery(){for(i=0;i<mndFileds.length;i++){ var fieldObj=document.forms['WebToLeads931413000000070001'][mndFileds[i]];if(fieldObj) {if(((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0){alert(fldLangVal[i] +' cannot be empty'); fieldObj.focus(); return false;}else if(fieldObj.nodeName=='SELECT'){if(fieldObj.options[fieldObj.selectedIndex].value=='-None-'){alert(fldLangVal[i] +' cannot be none'); fieldObj.focus(); return false;}} else if(fieldObj.type =='checkbox'){ if (fieldObj.checked == false){     alert('Please accept  '+fldLangVal[i]); fieldObj.focus();return false;}}}}}</script>  
				
				</form>
			</div><!-- #crmWebToEntityForm -->	
		</div>
	</div><!-- #leftcontact -->
	<div id="rightcontact" class="col-xs-24 col-sm-12">
		<div id="rightcontact_content">
			<span class="emailus text">EMAIL US</span>
			<span class="email text"><a href="mailto:hello@hatchit.co?Subject=Hello" target="_top">hello@hatchit.co</a></span>
			<span class="alsocall text">You can also give us a call</span>
			<span class="alsocallnum text">347.464.9656</span>
			<span class="dropby text">Or drop by and say hello</span>
			<span class="dropbyaddresstitle text">Hatchit</span>
			<span class="dropbyaddress text">11015 Perkins Road, Baton Rouge, LA 70810</span>		
			<ul class="sociallinks col-xs-24 col-sm-24">
				<li class="twitter col-xs-24 col-sm-8"><img src="img/twitter.png" alt="Hatchit Twitter"/><a href="twitter.com/hatchit">@hatchit</a></li>
				<li class="instagram col-xs-24 col-sm-8"><img src="img/instagram.png" alt="Hatchit Instagram"/><a href="instagram.com/hatchit">@hatchit</a></li>
				<li class="facebook col-xs-24 col-sm-8"><img src="img/facebook.png" alt="Hatchit Facebook"/><a href="facebook.com/hatchit">facebook.com/hatchit</a></li>
			</ul>
		</div><!-- #rightcontact_content -->
	</div><!-- #rightcontact -->
	<div id="map" class="">
	
	</div>
     <script>
	    var msg = getParameterByName('msg');
	    if(msg != '') {
	    	
	    }
	    	
	    	
	    function getParameterByName(name) {
			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
		    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		}
				
     </script>
     
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script>
		function initialize() {
		  var myLatlng = new google.maps.LatLng(30.374545,-91.089694);
		  var mapOptions = {
		    zoom: 16,
		    center: myLatlng,
		    scrollwheel: false,
		    navigationControl: false,
		    mapTypeControl: false,
		    scaleControl: false,
		    draggable: false,
		    disableDefaultUI: true
		  }
		  var map = new google.maps.Map(document.getElementById('map'), mapOptions);
		
		  var marker = new google.maps.Marker({
		      position: myLatlng,
		      map: map,
		      title: 'Hello World!'
		  });
		}
		
		google.maps.event.addDomListener(window, 'load', initialize);

    </script>     
<?php include 'footer.php'; ?>