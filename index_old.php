<?php include 'header.php'; ?>
    
        <div class="the-fold fill-screen">
            
            <div class="overlay">
                
            <?php include 'nav.php'; ?>

            <section class="intro">
                <h1 id="h1_headline" class="fadein1 fadein">Hi, we’re Hatchit.</h1>
                <h2 id="h2_headline" class="fadein2 fadein">We build websites, apps and branding
                designed to get you noticed.</h2>
                
                <br>
                <br>
                <p class="fadein3 fadein">We recently built an interactive website for TEDXLSU</p>
                <a href="#" class="fadein3 fadein btn btn-success">See TEDxLSU.com</a>
                <a href="#" class="fadein3 fadein btn btn-info">Get a Quote</a>

            </section>

            </div><!-- /.overlay -->

        </div><!-- /.the-fold -->
        
        <section class="services">
            <div class="services-header">
                <span class="how-can-we-help">How can we help you?</span>
                <span class="im-here-because">I’m Here Because I Need To:</span>
            </div><!-- /.services-header -->
            <div class=" service-listing">
                <div class="row">
                    <div class="col-xs-24 col-sm-8">
                        <div class="service-icon">
                            <img src="img/build-a-website-with-hatchit.png" alt="Build a Website with Hatchit">
                        </div><!-- /.service-icon -->
                        <h3>Build a Website</h3>
                        
                        <div class="service-desc">
                             Websites are our bread and butter. Without getting into the technical lingo, a Hatchit website is beautiful, functional, and easy to use.
                         </div><!-- /.service-desc --> 
                        <br>
                        <a href="#">See what we did for Arkel Constructor</a>


                    </div><!-- /.col-xs-24 col-sm-8 -->
                    <div class="col-xs-24 col-sm-8">
                        <div class="service-icon">
                            <img src="img/build-an-app-with-hatchit.png" alt="Build a Web App with Hatchit">
                        </div><!-- /.service-icon -->
                        <h3>Build a web app</h3>
                        <div class="service-desc">
                            If you have a great idea, we can bring it to digital life. We develop high quality appications and products for the web.
                        </div><!-- /.service-desc -->
                        <br>
                        <a href="#">See what we did for Rent Square</a>


                    </div><!-- /.col-xs-24 col-sm-8 -->
                    <div class="col-xs-24 col-sm-8">
                        <div class="service-icon">
                            <img src="img/build-a-brand-with-hatchit.png" alt="Build a Brand with Hatchit">
                        </div><!-- /.service-icon -->
                        <h3>Build a Brand</h3>
                        <div class="service-desc">
                            Getting you noticed is our specialty. Our creative team will help you build a brand that connects with audiences and sets you apart.
                        </div><!-- /.service-desc -->
                        <br>
                        <a href="#">See what we did for HP Serve</a>


                    </div><!-- /.col-xs-24 col-sm-8 -->
                </div><!-- /.row -->
            </div><!-- /. service-listing -->

        </section>
        
        <section class="from-the-blog">
            <div class="ftb-title">From the blog</div>

            <div class="blog-title">
                Conference Room Camera Woes
            </div><!-- /.blog-title -->

            <div class="blog-excerpt">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
            </div><!-- /.blog-excerpt -->

            <a href="#" class="btn-modern">Read</a>
        </section>

        <section class="we-hatchit container-fluid">
            <div class="row">
                <div class="col-xs-24 col-sm-12 social-col">
                    <div class="row">
                        <div class="col-xs-24">
                            <div class="we-hatchit-wrapper">
                                <span class="we-are">We are</span>
                                <span class="comp-name">Hatchit</span>
                                <h2>We build beautiful, people-friendly websites, apps and branding content.</h2>
                                <p>In short, we are a creative agency that specializes in building awesome, people-friendly websites and brand experiences for our clients (both big and small). We don’t get too specific because we understand that every client has a unique need. We aim to meet these needs by staying open minded and flexible in our service offerings, and we believe that our team is sharp (and friendly) enough to intelligently provide a solution to just about anything they can throw our way. </p>
                                <br><br>
                                <a href="#" class="btn btn-danger">About Us</a>
                            </div><!-- /.we-hatchit-wrapper -->
                        </div><!-- /.col-xs-24 -->
                    </div><!-- /.row -->
                </div><!-- /.col-xs-24 col-sm-12 -->
                <div class="col-xs-24 col-sm-12">
                    <div class="row social-media">
                        <div class="col-xs-12 facebook">
                            <span class="genericon genericon-facebook-alt"></span>
                        </div><!-- /.col-xs-12 -->
                        <div class="col-xs-12 twitter">
                            <span class="genericon genericon-twitter"></span>
                        </div><!-- /.col-xs-12 -->
                        <div class="col-xs-12 instagram">
                            <span class="genericon genericon-instagram"></span>
                        </div><!-- /.col-xs-12 -->
                        <div class="col-xs-12 linkedin">
                            <span class="genericon genericon-linkedin"></span>
                        </div><!-- /.col-xs-12 -->
                    </div><!-- /.row -->
                </div><!-- /.col-xs-24 col-sm-12 -->
            </div><!-- /.row -->    
            
        </section>

 <?php include 'footer.php'; ?>