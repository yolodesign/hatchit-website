
/**
 * Define exists function
 */
jQuery.fn.exists = function() {
    return this.length > 0;
}

jQuery.noConflict();

jQuery(function($){
    $('.meet-team a.prev-slide').on('click', function (event) {
        event.preventDefault()
        $('.meet-team').flexslider("prev") 
        $('.meet-team a.prev-slide p').addClass('faded')
    });
    $('.meet-team a.next-slide').on('click', function (event) {
        event.preventDefault()
        $('.meet-team').flexslider("next") 
        $('.meet-team a.next-slide p').addClass('faded')
    });
    $('.meet-team').flexslider({
        animation: "slide",
        // animationLoop: true,
        // reverse: true,
        controlNav: false,
        directionNav: false,
        after: function(){
        var active_rel = $('.flex-active-slide').prev().attr('rel');
        // alert(active_rel)
        $('.meet-team a.prev-slide p').text(active_rel)
        $('.meet-team a.prev-slide p').removeClass('faded')
        }
      });
    setTimeout(function(){
        $('body').addClass('activate')

    },400);
     $.stellar({
        // scrollProperty: 'transform',
        positionProperty: 'transform',
        hideDistantElements: false
     });
    $(window).resize(function(){
        $winheight = $(window).height()
        $aboutheight = $('.about-intro').height();
        $('.about-header, .intro-bg').css('height', $winheight);
        // $('.about-intro').css('marginTop', ($winheight/2)-$aboutheight);

    });
    $(window).resize();
    $(window).on('scroll', function(event) {
        event.preventDefault();
        $title = $('.title-wrap')
        $loc = $('.title-wrap').scrollTop()
        // console.log($title.position().top)
        $aboutheight = $('.about-intro').height();
        $titlePos = ($aboutheight/2)-89
        $title.css({
            marginTop: $titlePos+'px',
            opacity: 1
        })
        pos = $title.position().top
        // if(pos >150){
        //     op = 2-((pos*1.3)/200)
        //     trans = (op > 0) ? op : 0
        //     $title.css('opacity', trans);
        // }else{
        //     $title.css('opacity', 1);
        // }
    }); 
    $('.meet-team').bind('inview', function (event, visible) {
        if (visible) {
            $('.meet-team').addClass('slider-active')            
        }else{
            // $('.meet-team').removeClass('slider-active')
        }
    });
    $('#maps').bind('inview', function(){
        $('.pin').each(function(index){
            $(this).addClass('inview t'+(index+1))    
        })
    });
      $('#map').bind('inview', function (event, visible) {
        if (visible) {
          $('.pin').each(function(index){
            $(this).addClass('inview t'+(index+1))    
            })
        } else {
          // $('.pin').removeClass('inview')
        }
      });
})


jQuery(function($, w, undefined) {
    var body = $('body'),
        _window = $(window),
        wwidth,
        wheight,
        stime;

    var wb = {

        getsizes: function() {
            wheight = _window.height();
            wwidth = _window.width();
        },
        fillscreen: function() {
            if (wheight < 250) {
                theheight = 250;
            } else {
                theheight = wheight;
            }
            $('.fill-screen').width(wwidth).height(theheight);
        },
        wscroll: function() {
            // homepage parallax
            scrolldist = Math.floor($(document).scrollTop());
            if (!Modernizr.touch) {
                $('.slide').css({
                    'margin-top': scrolldist / 2 + 'px'
                });
            }
        },
        wresize: function() {
            if (jQuery('#content').length > 0) {
                navLeftOffset = jQuery('#content').offset().left + jQuery('#content').width() + (.04 * jQuery('#content').width());
                setFixed();
            }
            wb.getsizes();
            wb.fillscreen();
            //wb.setupportfolio();
            wb.setupresources();
            $('#sizes').text('w:' + wwidth + 'px h:' + wheight + 'px');

            if ($('nav.portfolio-nav').length) {
                pnavscrollpos = $('nav.portfolio-nav').offset().top;
            }

            if (wwidth > 859) {
                $('body').removeClass('mobile-nav-active');
                $('#mobile-nav').removeClass('active');
            }
        },
        internalresize: function() {
            if (!Modernizr.touch && jQuery('.entry-content-wrapper').exists()) {
                jQuery('.entry-content-wrapper').css('height', jQuery('html').height() - jQuery('.entry-content-wrapper').offset().top);
            }
        },
        changeslide: function(direction, slider) {
            // set slider object
            if (slider == '') {
                slider = $('#featured-slider');
            } else {
                slider = $(slider);
            }

            // if no direction, set default
            if (!direction) {
                direction = 'next';
            }

            var current = $('.slide.current'),
                next = current.next('.slide'),
                prev = current.prev('.slide'),
                count = slider.find('.slide').length;

            if (!next.length) {
                next = $('.slide').first();
            }
            if (!prev.length) { // if there isn't a previous slide
                prev = $('.slide').last();
            }

            if (direction == 'next') {
                next.removeClass('past').addClass('current');
                current.removeClass('current active').addClass('past');
                next.addClass('active');
                current = next;
            } else {
                prev.removeClass('past').addClass('current');
                current.removeClass('current active').addClass('past');
                prev.addClass('active');
                current = prev;
            }

            clearInterval(stimer);
            stimer = setInterval(function() {
                wb.changeslide();
            }, 7000);


            wb.changeslidedata();
        },
        changeslidedata: function() {
            var current = $('.slide.current'),
                next = current.next('.slide'),
                prev = current.prev('.slide');

            if (!next.length) {
                next = $('.slide').first();
            }
            if (!prev.length) { // if there isn't a previous slide
                prev = $('.slide').last();
            }

            $('.controls .inner .caption a span').text(current.attr('title'));

            if (current.attr('data-url') != '#') {
                $('.controls .inner .caption a').attr('href', current.attr('data-url'));
                $('.controls .inner .caption a .view_this_proj').fadeIn();
            } else {
                $('.controls .inner .caption a').attr('href', '#');
                $('.controls .inner .caption a .view_this_proj').hide();
            }


            $('.controls .headline').fadeOut('fast', function() {
                jQuery(this).text(current.attr('data-headline')).fadeIn(3000);
            });
        },
        setupresources: function() {
            if ($('article.post').length && $(".bricklink").length) {
                var cw = (function() {
                    if ($(window).width() < 860) {
                        return $("#resource-masonry").width();
                    } else {
                        return ($("#resource-masonry").width() / 3) - 20;
                    }
                }());
                $("article.post").css("width", cw);
                $('#resource-masonry').masonry({
                    itemSelector: 'article.post',
                    columnWidth: cw,
                    isAnimated: true,
                    gutterWidth: 20
                });
            }
        },
        setupslider: function() {

            wb.changeslidedata();
            stimer = setInterval(function() {
                wb.changeslide();
            }, 7000);
        },
        init: function() {
            // if we're on the homepage
            if ($('.home').exists()) {
                //wb.setupslider();
                // wb.setupmap();
                wb.fillscreen();
                wb.wresize();
            }
            wb.setupresources();
        }

    } 

    if (body.hasClass('home')) {
            wb.init();

            $(window).on('resize', wb.wresize);

            if (wwidth > 767) {
                $(window).on('scroll', wb.wscroll);
            }
        } else {
            $(window).scroll(function() {
                setFixed();
            });
            $(window).on('resize', wb.internalresize);
        }

    //Headline Responsive Text
    $("#h1_headline").fitText(1, { minFontSize: '20px', maxFontSize: '72px' });
    $("#h2_headline").fitText(1, { minFontSize: '15px', maxFontSize: '60px' });

    
    /* HOMEPAGE VIDEO
    ----------------------------------------- */
    $('.the-fold').videoBG({
        mp4:'video/TEDX-BG-VIDEO.mp4',
        ogv:'video/TEDX-BG-VIDEO.ogv',
        webm:'video/TEDX-BG-VIDEO.webm',
        poster:'video/TEDX-BG-VIDEO.jpg',
        scale:true,
        zIndex:0
    });

    /* OUR WORK Filter
    ----------------------------------------- */
    $('#Container').mixItUp();

});	