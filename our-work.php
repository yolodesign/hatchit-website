<?php include 'header.php'; ?>

<div class="our-work-header">
	<?php include 'nav.php'; ?>	
</div><!-- /.our-work-header -->	
<section class="featured-work">
	<a href="#" class="featured-work-item" style="background-image: url('portfolio/k2/bkg-k2-coolers.jpg');">
		<div class="featured-work-wrapper">
			<div class="row">
				<div class="col-xs-24 col-sm-12 featured-work-title">
					<div class="featured-work-content">
						<h2>K2 Coolers</h2>
						<p>Fully responsive, fully rebranded web-store.</p>
					</div><!-- /.eatured-work-content -->
				</div><!-- /.col-xs-24 col-sm-12 -->
				<div class="col-xs-24 col-sm-12 featured-work-img">
					<img src="portfolio/k2/k2-coolers-website.png" alt="" style="right: -19px;">
				</div><!-- /.col-xs-24 col-sm-12 -->
			</div><!-- /.row -->
			<div class="bkg-overlay k2-coolers">
				<div class="content_bottom">
					<ul>
						<li>WEB UX/UI</li>
						<li>E-COMMERCE</li>
						<li>RESPONSIVE</li>
						<li>CUSTOM DESIGN</li>
						<li>MAGENTO</li>
					</ul>
					<br><br><br>
					<div class="fake_button">
						<span>View Case Study</span><span class="gt_arrow">&gt;</span>
					</div><!-- /.fake_button -->
				</div><!-- /.content_bottom -->
			</div><!-- /.bkg-overlay -->
		</div><!-- /.featured-work-wrapper -->
	</a><!-- /.featured-work-item -->

	<a href="#" class="featured-work-item" style="background-image: url('portfolio/hp-serve/bkg-hp-serve.jpg');">
		<div class="featured-work-wrapper">
			<div class="row">
				<div class="col-xs-24 col-sm-12 featured-work-title">
					<div class="featured-work-content">
						<h2>HP Serve</h2>
						<p>Full scale brand reboot for a powerful non-profit.</p>
					</div><!-- /.eatured-work-content -->
				</div><!-- /.col-xs-24 col-sm-12 -->
				<div class="col-xs-24 col-sm-12 featured-work-img">
					<img src="portfolio/hp-serve/hp-serve-branding-and-web.png" alt="">
				</div><!-- /.col-xs-24 col-sm-12 -->
			</div><!-- /.row -->
			<div class="bkg-overlay hp-serve">
				<div class="content_bottom">
					<ul>                            
						<li>IDENTITY</li>
						<li>WEB UX/UI</li>
						<li>RESPONSIVE</li>
						<li>ONLINE DONATIONS</li>
						<li>BRAND COLLATERAL</li>
					</ul>
					<br><br><br>
					<div class="fake_button">
						<span>View Case Study</span><span class="gt_arrow">&gt;</span>
					</div><!-- /.fake_button -->
				</div><!-- /.content_bottom -->
			</div><!-- /.bkg-overlay -->
		</div><!-- /.featured-work-wrapper -->
	</a><!-- /.featured-work-item -->

	<a href="#" class="featured-work-item" style="background-image: url('portfolio/cma/bkg-cma.jpg');">
		<div class="featured-work-wrapper">
			<div class="row">
				<div class="col-xs-24 col-sm-12 featured-work-title">
					<div class="featured-work-content">
						<h2>CMA Technology</h2>
						<p>A new online presence designed to drive business.</p>
					</div><!-- /.eatured-work-content -->
				</div><!-- /.col-xs-24 col-sm-12 -->
				<div class="col-xs-24 col-sm-12 featured-work-img">
					<img src="portfolio/cma/cma-website-seo-strategy.png" alt="">
				</div><!-- /.col-xs-24 col-sm-12 -->
			</div><!-- /.row -->
			<div class="bkg-overlay cma-tech">
				<div class="content_bottom">
					<ul>
						<li>CONTENT BUILDING</li>
						<li>WEB UX/UI</li>
						<li>RESPONSIVE</li>
						<li>SEO STRATEGY</li>
					</ul>
					<br><br><br>
					<div class="fake_button">
						<span>View Case Study</span><span class="gt_arrow">&gt;</span>
					</div><!-- /.fake_button -->
				</div><!-- /.content_bottom -->
			</div><!-- /.bkg-overlay -->
		</div><!-- /.featured-work-wrapper -->
	</a><!-- /.featured-work-item -->

	<a href="#" class="featured-work-item bottom-none" style="background-image: url('portfolio/lsu/bkg-lsu.jpg');">
		<div class="featured-work-wrapper">
			<div class="row">
				<div class="col-xs-24 col-sm-12 featured-work-title">
					<div class="featured-work-content">
						<h2>LSU</h2>
						<p>A contemporary web presence for the LSU College of Art + Design.</p>
					</div><!-- /.eatured-work-content -->
				</div><!-- /.col-xs-24 col-sm-12 -->
				<div class="col-xs-24 col-sm-12 featured-work-img">
					<img src="portfolio/lsu/lsu-art-design-website.png" alt="">
				</div><!-- /.col-xs-24 col-sm-12 -->
			</div><!-- /.row -->
			<div class="bkg-overlay lsu-overlay">
				<div class="content_bottom">
					<ul>
					<li>WEB UX/UI</li>
					<li>E-COMMERCE</li>
					<li>RESPONSIVE</li>
					<li>CUSTOM DESIGN</li>
					<li>MAGENTO</li>
					</ul>
					<br><br><br>
					<div class="fake_button">
						<span>View Case Study</span><span class="gt_arrow">&gt;</span>
					</div><!-- /.fake_button -->
				</div><!-- /.content_bottom -->
			</div><!-- /.bkg-overlay -->
		</div><!-- /.featured-work-wrapper -->
	</a><!-- /.featured-work-item -->
</section>
<section class="all-work">
	<div class="want_more">
		Want Even More?
	</div><!-- /.want_more -->
	<div class="work_filter">
		<ul>
			<li class="filter" data-filter="all"><a class="current" href="#all">All</a></li>
			<li class="filter" data-filter=".web"><a href="#web">Web</a></li>
			<li class="filter" data-filter=".print"><a href="#print">Print</a></li>
			<li class="filter" data-filter=".identity"><a href="#identity">Identity</a></li>
			<li class="filter" data-filter=".applications"><a href="#application">Application</a></li>
			<li class="filter" data-filter=".miscellaneous"><a href="#miscellaneous">Miscellaneous</a></li>
		</ul><!-- /.work_filter -->
	</div><!-- /.work_filter -->
	
	<div id="Container" class="row work_wrapper">
		
		<div class="mix all col-xs-24 col-sm-8">
			<a href="#" class="work" style="background-image: url(img/Hatchit-Web-Sites-Baton-Rouge-partners-with-CCs-Coffee.jpg);">
				<div class="ccs_overlay work_overlay">
					<div class="animated fadeInDown">
						Connecting Followers to CC’s Food Truck
						<div class="view_project">View Project <span>&gt;</span></div>
					</div>
				</div><!-- /.overlay -->
				<div class="work_logo">
					<img src="img/Hatchit-Branding-Baton-Rouge-partners-with-CCs-Coffee.png" alt="Hatchit Web Sites Baton Rouge partners with CCs Coffee">
				</div><!-- /.work_logo -->
			</a><!-- /.work -->
		</div><!-- /.col-xs-24 col-sm-8 -->

		<div class="mix all print col-xs-24 col-sm-8">
			<a href="#" class="work" style="background-image: url(img/Hatchit-Websites-Baton-Rouge-partners-with-TED-x-LSU.jpg);">
				<div class="tedx_overlay work_overlay">
					<div class="animated fadeInDown">
						A forward-thinking website for ambitious minds.
						<div class="view_project">View Project <span>&gt;</span></div>
					</div>
				</div><!-- /.overlay -->
				<div class="work_logo">
					<img src="img/Hatchit-Branding-Baton-Rouge-partners-with-TED-x-LSU.png" alt="Hatchit Websites Baton Rouge partners with TED x LSU">
				</div><!-- /.work_logo -->
			</a><!-- /.work -->
		</div><!-- /.col-xs-24 col-sm-8 -->
		
		<div class="mix all web col-xs-24 col-sm-8">
			<a href="#" class="work" style="background-image: url(img/Hatchit-partners-with-Garder-Chrisian-Community-School-in-Baton-Rouge.jpg);">
					<div class="gardere_overlay work_overlay">
						<div class="animated fadeInDown">
							Branding and web design for a community school.
						<div class="view_project">View Project <span>&gt;</span></div>
						</div>
					</div><!-- /.overlay -->
					<div class="work_logo">
						<img src="img/Hatchit-Branding-partners-with-Garder-Chrisian-Community-School-in-Baton-Rouge.jpg" alt="Hatchit Website Baton Rouge partners with Garder Chrisian Community School">
					</div><!-- /.work_logo -->
			</a><!-- /.work -->
		</div><!-- /.col-xs-24 col-sm-8 -->
	</div><!-- /.row -->
	
	
</section>
<?php include 'footer.php'; ?>